Work for the SSAD project has been divided as follows:
Frontend - Naman Singhal, Hitesh Sharma, Kaja Varun Kumar
Backend - Sambuddha Basu, Aishwary Gupta

The work will begin from start of 3rd week of September.
By the first week, we would like to get the basic functionalities up and running. These include the routes to all the pages and a basic view.
By end of second week, database integration into the website. Also, Facebook authentication will be added during this time.
Third week - Matching partners algorithm. Facebook likes will be evaluated and based on that, compatibility tests will be run. Search by name, caste, age group work has to get started around this time.
Fourth week - Completion of the search feature. Tests have to be run to check whether the API’s are working fine. Each API call should get the required data and appropriate responses should be sent either wise.
Fifth week - Deployment on the main server. All the tests should be passing and any cleanup work left has to be completed around this ti me.
NOTE:-Instead of facebook likes we are searching from general way of mode.Because fb show some sort of authentication problems.
Links for the front-end:
http://bootswatch.com/
http://bootflat.github.io/
For getting the facebook likes for users:
https://developers.facebook.com/docs/graph-api/reference/v2.1/user/likes
For finding corresponding categories for the different facebook pages:
https://developers.facebook.com/docs/graph-api/reference/v2.1/page
Facebook login docs can be found here:
https://developers.facebook.com/docs/facebook-login/login-flow-for-web/v2.1
User privacy:
--- Report to the admin about a user.
--- Blocking users.
Profile can be private/public. Based on privacy, profile data has to be shown.
A user can accept/decline to speak with some other user. Once they accept, lead them towards facebook chat.
Admin roles(implement decorator and wrappers around other views):
	CRUD for users.
	Define quizzes to get to know what users are really into.
	Week-wise project completion plan:
	Week 1 -> Complete layouts of all the pages. Freeze the requirements. 
	Complete requirement document. Routes to all the pages. This includes controlling access to users.
	Week 2 -> Database integration. Facebook APIs will be used for authentication. Facebook likes will be taken into account. Appropriate error handling pages when user is not authenticated. Admin roles have to be specified by this time.
	Week 3 -> Matching partners algorithm. Search by name, caste, age group work has to get started around this time. Results will be shown based on location.
	Week 4 -> Tests have to be run to check whether the API’s are working fine. Each API call should get the required data and appropriate responses should be sent either wise.
	Layout:
	Search bar on the top, with login(preferably)
	Things required at the beginning:
	Facebook login
	Search feature at the top
	What we do

	Payment gateway for the users. 


	Admin Requirements:

	1) Admin should be list see the users who are using the website. - View/Edit/Delete profiles.
	2) Search should be enabled for admin to view the users by "name" "Age" "Case".. and other parameters
	3) Admin should be able to create quizzes and post it to all users / user groups.
	4) Depending on the data in profiles ...groups should be created on basis of Mother Tongue, State , Caste, Religion etc
	5) View the details of payment. Information of subscription expiry and etc of the users.
	6) Notification mails to selected users

	Group Members:
	Sambuddha Basu - sambuddhabasu1@gmail.com, sambuddha.basu@students.iiit.ac.in
	Hitesh Sharma - hitesh.sharma@students.iiit.ac.in
	Naman Singhal - naman.singha@students.iiit.ac.in
	Aishwary Gupta - aishwary.gupta@students.iiit.ac.in
	Kaja Varun Kumar - kajavarun.kumar@students.iiit.ac.in
