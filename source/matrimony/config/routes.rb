Rails.application.routes.draw do
  
  get 'home' => "index#home"
  get 'sessions/destroy' => "sessions#destroy"
  get 'sessions/create' => "sessions#create"
  get 'user_profile/:id' => 'user#user_profile'
  post 'user_profile_update' => 'user#user_profile_update'
  get 'user_profile_facebook_update' => 'user#user_profile_facebook_update'
  match 'search' => 'user#search', :via => ["get", "post"]
  post "create_image" => "user#create_image"
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'
  resources :sessions, only: [:create, :destroy]
  root to: "index#home"
  get 'add_image/:id' => "user#add_image"
  match '/set_profile_image' => "user#set_profile_image", :via => ["get", "post"]
  match '/delete_image' => "user#delete_image", :via => ["get", "post"]
  get "/admin" => "admin#index"
  get "/admin/users" => "admin#users"
  get "/admin/message" => "admin#message"
  get "/admin/reports" => "admin#reports"
  get "/admin/delete/:id" => "admin#delete"
  get "/view_image/:id" => "user#view_image"
  get "/show_interest/:by/:to" => "user#show_interest"
  get "/interest_shown_by" => "user#interest_shown_by"
  get "/interest_shown_to" => "user#interest_shown_to"
  get "/report" => "user#report"
  get "/message" => "user#message"
  get "/show_message/:id" => "user#show_message"
  post '/new_message/:id' => "user#new_message"
  
end
