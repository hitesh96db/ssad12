# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141117182710) do

  create_table "images", force: true do |t|
    t.integer  "user_id"
    t.string   "img_file_name"
    t.string   "img_content_type"
    t.integer  "img_file_size"
    t.datetime "img_updated_at"
    t.integer  "profile"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.integer  "user_1"
    t.integer  "user_2"
    t.datetime "timestamp"
    t.string   "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reports", force: true do |t|
    t.integer  "user1_id"
    t.integer  "user2_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "email"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "location"
    t.text     "description"
    t.string   "phone"
    t.string   "salary"
    t.string   "gender"
    t.string   "caste"
    t.string   "education"
    t.string   "work"
    t.string   "language"
    t.string   "color"
    t.string   "religion"
    t.string   "hobbies"
    t.string   "date"
    t.string   "month"
    t.string   "year"
    t.integer  "age"
    t.string   "height"
    t.string   "weight"
    t.string   "interest_to"
    t.string   "interest_by"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
