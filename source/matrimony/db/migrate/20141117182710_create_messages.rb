class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :user_1
      t.integer :user_2
      t.datetime :timestamp
      t.string :message

      t.timestamps
    end
  end
end
