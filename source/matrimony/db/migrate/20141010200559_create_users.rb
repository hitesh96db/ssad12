class CreateUsers < ActiveRecord::Migration
  def change
#    drop_table :users
    create_table :users do |t|
      t.string :provider
      t.string :uid
      t.string :name
      t.string :email
      t.string :firstname
      t.string :lastname
      t.string :location
      t.text :description
      t.string :phone
      t.string :salary
      t.string :gender
      t.string :caste
      t.string :education
      t.string :work
      t.string :language
      t.string :color
      t.string :religion
      t.string :hobbies
      t.string :date
      t.string :month
      t.string :year
      t.integer :age
      t.string :height
      t.string :weight
      t.string :interest_to
      t.string :interest_by
      t.string :oauth_token
      t.datetime :oauth_expires_at
      t.timestamps
    end
  end
end
