class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :user_id
      t.attachment :img
      t.integer :profile
      t.timestamps
    end
  end
end
