class Image < ActiveRecord::Base
    has_attached_file :img, :styles => { :large => "500x530", :medium => "300x330>", :small => "250x250>", :thumb => "200x200>" }, :default_url => "/images/default.jpg"
    validates_attachment_content_type :img, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
    validates_attachment_file_name :img, :matches => [/png\Z/, /jpe?g\Z/, /gif\Z/]
end
