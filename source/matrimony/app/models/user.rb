class User < ActiveRecord::Base
    def self.from_omniauth(auth)
        where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
            user.provider = auth.provider 
            user.uid = auth.uid
            user.name = auth.info.name
            user.email = auth.info.email
            user.firstname = auth.info.first_name
            user.lastname = auth.info.last_name
            user.location = auth.info.location
            user.description = auth.info.description
            user.phone = auth.info.phone
#           user.gender = auth.info.gender
#           user.date = auth.info.date
#           user.month = auth.info.month
#           user.year = auth.info.year
#           user.language = auth.info.language
#           user.caste = auth.info.caste
#           user.salary = auth.info.salary
#           user.education = auth.info.education
            # Access token information.
        user.oauth_token = auth.credentials.token
        user.oauth_expires_at = Time.at(auth.credentials.expires_at)
            user.save
        end
    end
end
