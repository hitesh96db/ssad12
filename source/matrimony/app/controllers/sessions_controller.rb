class SessionsController < ApplicationController
  def create
    user = User.from_omniauth(env["omniauth.auth"])
    if user.date != ".." && user.month != ".."  && user.year != ".." 
      require 'time_diff'
      if user.date and user.month and user.year
        @age = Time.diff(Time.now, Time.parse(user.date.to_s + "/" + user.month.to_s + "/" + user.year.to_s))[:year].to_i
        user.age = @age
        user.save
      end
    end
    session[:user_id] = user.id
    session[:user_name] = user.firstname
    @user = User.find(user.id)
    if @user.firstname == "Mataddmin"
        redirect_to "/admin"
    else 
        if ( @user.gender == "Select" || @user.gender == nil ) ||
         ( @user.caste == "Select" || @user.caste == nil ) ||
         ( @user.location == "Select" || @user.location == nil ) ||
         ( @user.language == "Select" || @user.language == nil ) ||
         ( @user.religion == "Select" || @user.religion == nil ) ||
         ( @user.date == ".." || @user.date == nil ) ||
         ( @user.month == ".." || @user.month == nil ) ||
         ( @user.year == ".." || @user.year == nil ) ||
         ( @user.phone == nil || @user.phone == nil )
            flash[:alert] = "Please fill in the necessary details."
            redirect_to get_profile_link
        else
            redirect_to root_url
        end
    end
  end

  def destroy
    session[:user_id] = nil
    session[:user_name] = nil
    flash[:notice] = "Logged out!"
    redirect_to root_path
  end
end
