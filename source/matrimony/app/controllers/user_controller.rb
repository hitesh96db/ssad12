class UserController < ApplicationController
  
  def user_profile
    id = params[:id].to_i
    @user = User.find(id)
    @image = Image.where(:user_id => id, :profile => 1).first
    if !session[:user_id]
        flash[:alert] = "Not logged in!"
        redirect_to root_url
    end
  end
  
  def create_image
    @image = Image.create( image_params )
    @current = Image.where(:user_id => session[:user_id])
    no_of_images = Image.where(:user_id => session[:user_id]).count

    if no_of_images < 4
        @image.user_id = session[:user_id]
        @image.profile = 0
        if @image.save
            flash[:notice] = "Image Added!"
        else
            flash[:alert] = "Error! Please try again later"
        end
    else
        flash[:alert] = "More than Four Images are not allowed!"
    end
    redirect_to "/add_image/" + session[:user_id].to_s
  end
  
  def add_image
    id = params[:id].to_i
    @images = Image.where(:user_id => id)
    if not @images
        render :text => "No Image"
    end
  end
  
  def view_image
    id = params[:id].to_i
    @user = User.find(id)
    @images = Image.where(:user_id => id)
    if not @images
        render :text => "No Image"
    end
  end
  
  def set_profile_image
    no = params["pic_no"].to_i - 1
    @images = Image.where(:user_id => session[:user_id])
    k = 0
    @images.each do |i|
        if k == no
            i.profile = 1
            i.save
        else
            i.profile = 0
            i.save
        end
    k = k + 1
    end
    flash[:notice] = "Profile Image changed!"
    redirect_to get_profile_link
  end
  
  def delete_image
    no = params["pic_no"].to_i
    @images = Image.where(:user_id => session[:user_id])
    @images[no-1].delete
    flash[:notice] = "Image deleted!"
    redirect_to "/add_image/" + session[:user_id].to_s
  end
  def user_profile_update
    @user = User.find(session[:user_id])
    if @user.update_attributes(:firstname => params[:firstname], :work => params[:work], :weight => params[:weight], :height => params[:height], :lastname => params[:lastname], :hobbies => params[:hobbies], :religion => params[:religion], :color => params[:color], :description => params[:description], :year => params[:year], :month => params[:month], :date => params[:date], :language => params[:language], :education => params[:education], :caste => params[:caste], :gender => params[:gender], :salary => params[:salary], :location => params[:location], :phone => params[:phone])
        flash[:notice] = "Profile updated!"
        redirect_to get_profile_link
    else
      error_message("Error! Please try again later")
      render "user_profile"
    end
  end
  
  def user_profile_facebook_update
    require 'net/http'
    require 'json'
    @user = User.find(session[:user_id])
    @facebook_graph_url = 'https://graph.facebook.com/v2.1/me?fields=first_name,last_name&access_token=' + @user.oauth_token
    url = URI.parse(@facebook_graph_url)
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Get.new(url.request_uri)
    res = http.request(request)
    res = JSON.parse(res.body)
  	  @user.update_attributes(:firstname => res['first_name'], :lastname => res['last_name'])
    redirect_to root_url
  end

  def show_interest
    @interest_by = params[:by]
    @interest_to = params[:to]
    @interest_by_user = User.find(@interest_by)
    @interest_to_user = User.find(@interest_to)
    if @interest_by_user.interest_to
      @interest_to_array = @interest_by_user.interest_to.split(',')
    else
      @interest_to_array = []
    end
    if not @interest_to_array.include? @interest_to
      @interest_to_array.push(@interest_to)
      @interest_by_user.interest_to = @interest_to_array.join(',')
      @interest_by_user.save
    end
    if @interest_to_user.interest_by
      @interest_by_array = @interest_to_user.interest_by.split(',')
    else
      @interest_by_array = []
    end
    if not @interest_by_array.include? @interest_by
      @interest_by_array.push(@interest_by)
      @interest_to_user.interest_by = @interest_by_array.join(',')
      @interest_to_user.save
    end
    redirect_to root_url
  end

  def interest_shown_by
    @users = []
    if current_user.interest_by
      @interest_by = current_user.interest_by.split(',')
      for @current_interest_by in @interest_by
        @user = User.where(:id => @current_interest_by)
        if @user[0] != nil
            @users.push(@user)
        end
      end
    end
  end

  def interest_shown_to
    @users = []
    if current_user.interest_to
      @interest_to = current_user.interest_to.split(',')
      for @current_interest_to in @interest_to
        @user = User.where(:id => @current_interest_to)
        if @user[0] != nil
            @users.push(@user)
        end
      end
    end
  end

  def message
    @users = []
    if current_user.interest_to
      @interest_to = current_user.interest_to.split(',')
      for @current_interest_to in @interest_to
        @user = User.where(:id => @current_interest_to)
        if @user[0] != nil
            @user = @user[0]
            @mutual_interest = 0
            if @user.interest_to
            @user_interest_to = @user.interest_to.split(',')
            for @user_current_interest_to in @user_interest_to
                if @user_current_interest_to.to_i == current_user.id.to_i
                    @mutual_interest = 1
                    break
                end
            end
	        end
        end
        end
    end
    if @mutual_interest == 1
        @users.push(@user)
    end
  end

  def show_message
    @user_1 = current_user
    @user_2 = User.find(params[:id])
    @messages_1 = Messages.where(:user_1 => @user_1.id, :user_2 => @user_2.id)
    @messages_2 = Messages.where(:user_1 => @user_2.id, :user_2 => @user_1.id)
    @messages = []
    for @message_1 in @messages_1
      @messages.push(@message_1)
    end
    for @message_2 in @messages_2
      @messages.push(@message_2)
    end
    @sorted_messages = @messages.sort_by {|k| k['timestamp']}
    @messages = @sorted_messages
  end

  def new_message
    @message_to = User.find(params[:id])
    @message_from = current_user
    @timestamp = Time.now
    @message_content = params[:message]
    print @message_content
    @message = Messages.new(:user_1 => @message_from.id, :user_2 => @message_to.id, :timestamp => @timestamp, :message => @message_content)
    @message.save
    redirect_to "/show_message/" + @message_to.id.to_s
  end

  def report
    current_id = params[:cid]
    user_id = params[:uid]
    @report = Report.where(:user1_id => current_id, :user2_id => user_id)
    if @report[0] != nil
        flash[:alert] = "User already reported"
    else
        @report = Report.new(:user1_id => current_id , :user2_id => user_id)
        @report.save
        flash[:notice] = "User reported!"
    end
    redirect_to "/interest_shown_by"
  end


  def search
    # check if user filled his own details before searching
    @user = User.find(session[:user_id])
    if ( @user.gender == "Select" || @user.gender == nil ) ||
       ( @user.caste == "Select" || @user.caste == nil ) ||
       ( @user.location == "Select" || @user.location == nil ) ||
       ( @user.language == "Select" || @user.language == nil ) ||
       ( @user.religion == "Select" || @user.religion == nil ) ||
       ( @user.date == ".." || @user.date == nil ) ||
       ( @user.month == ".." || @user.month == nil ) ||
       ( @user.year == ".." || @user.year == nil ) ||
       ( @user.phone == nil || @user.phone == "" )
        flash[:alert] = "Please fill in your 'necessary' profile details first."
        url = get_profile_link
        redirect_to url
    end
    # check if parameters are valid
    min_age = params[:min_age]
    max_age = params[:max_age]
    caste = params[:caste]
    language = params[:mother_tongue]
    location = params[:location]
    @users = User.all
    if min_age != "Min age" and max_age != "Max Age"
        @users = @users.where(:age => min_age.to_i..max_age.to_i)
    elsif min_age != "Min age" and max_age == "Max Age"
        @users = @users.where(:age => min_age.to_i..60)
    elsif min_age == "Min age" and max_age != "Max Age"
        @users = @users.where(:age => 18..max_age.to_i)
    end
    if location != "Select"
        @users = @users.where(:location => location)
    end
    if language != "Select"
        @users = @users.where(:language => language)
    end
    if caste != "Select"
        @users = @users.where(:caste => caste)
    end
    gender = "Male"
    if @user.gender == "Male"
    	gender = "Female"
    end
    @users = @users.where(:gender => gender)
  end
    
  private
  def image_params
    params.require(:image).permit(:img, :user_id)
  end
  
end
