class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user
  helper_method :get_min_age_options
  helper_method :get_max_age_options
  helper_method :get_location
  helper_method :get_caste
  helper_method :get_language
  helper_method :get_profile_link
  helper_method :get_link
  helper_method :current_id
  helper_method :has_image
  helper_method :get_image
  helper_method :get_gender
  helper_method :get_caste
  helper_method :get_language
  helper_method :get_location
  helper_method :get_religion
  helper_method :get_date
  helper_method :get_month
  helper_method :get_year
  helper_method :get_action_buttons
  helper_method :get_name
  helper_method :get_show_interest_link
  
  def get_name
    session[:user_name]
  end
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  def has_image(id)
    @image = Image.where(:user_id => id).first
    if @image == nil 
        return nil
    else
        return "ok"
    end
  end

  def get_image(id)
    @image = Image.where(:user_id => id, :profile => 1).first()
    return @image
  end
  
  def ok_message(message)
    flash[:notice] = ("<span class='glyphicon glyphicon-ok'></span>" + " " + message).html_safe
  end

  def error_message(message)
    flash[:alert] = ("<span class='glyphicon glyphicon-remove'></span>" + " " + message).html_safe
  end

  def get_min_age_options
    all = []
    temp = ["Min Age", "Min age"]
    all << temp
    (18..48).each do |i| 
        option = i
        value = i.to_s
        temp = [option, value]
        all << temp 
    end
    return all
  end

  def get_max_age_options
    all = []
    temp = ["Max Age", "Max Age"]
    all << temp
    (18..60).each do |i|
        option = i
        value = i.to_s
        temp = [option, value]
        all << temp
    end
    return all
  end

  def get_caste
    all = []
    list = ["Select", "Ad Dharmi", "Brahmin", "Choudhary", "Reddy", "Vaishas", "Baniya", "Agarwal", "Rajput"]
    list.each do |i|
        option = i
        value = i
        temp = [option, value]
        all << temp
    end
    return all
  end

  def get_language
    all = []
    list = ["Select", "English", "Hindi", "Telugu", "Tamil", "Urdu", "Marathi"]
    list.each do |i|
        option = i
        value = i
        temp = [option, value]
        all << temp
    end
    return all
  end

  def get_location
    all = []
    list = ["Select", "Delhi", "Mumbai", "Kolkata", "Hyderabad", "Chennai", "Jaipur", "Pune"]
    list.each do |i|
        option = i
        value = i
        temp = [option, value]
        all << temp
    end
    return all
  end

  def get_gender
    all = []
    list = ["Select", "Male", "Female"]
    list.each do |i|
        option = i
        value = i
        temp = [option, value]
        all << temp
    end
    return all
  end
  
  def get_religion
    all = []
    list = ["Select", "Hindu", "Muslim", "Christian", "Sikh", "Jain"]
    list.each do |i|
        option = i
        value = i
        temp = [option, value]
        all << temp
    end
    return all
  end

  def get_date
    all = []
    temp = ["..", ".."]
    all << temp
    (1..31).each do |i| 
        option = i
        value = i.to_s
        temp = [option, value]
        all << temp 
    end
    return all
  end

  def get_month
    all = []
    temp = ["..", ".."]
    all << temp
    (1..12).each do |i| 
        option = i
        value = i.to_s
        temp = [option, value]
        all << temp 
    end
    return all
  end

  def get_year
    all = []
    temp = ["..", ".."]
    all << temp
    (1955..1996).each do |i| 
        option = i
        value = i.to_s
        temp = [option, value]
        all << temp 
    end
    return all
  end
    
  def get_profile_link
    "/user_profile/" + session[:user_id].to_s
  end

  def get_link(id)
    "/user_profile/" + id.to_s
  end

  def get_show_interest_link(interest_by, interest_to)
    "/show_interest/" + interest_by.to_s + "/" + interest_to.to_s
  end

  def current_id
    session[:user_id]
  end
  
  def get_action_buttons(id)
    id = id.to_s
    btns = "<a class='btn btn-primary' role='button' href='/user_profile/" + id + "'>View Profile</a> | "
    btns = btns + "<a class='btn btn-warning' role='button' href='/admin/notify/" + id + "'>Message</a> | "
    btns = btns + "<a class='btn btn-danger' role='button' href='/admin/delete/" + id + "'>Delete</a> | "
    btns.html_safe
  end
end
