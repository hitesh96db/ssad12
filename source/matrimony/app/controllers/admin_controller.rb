class AdminController < ApplicationController
    def index
        @no_of_users = User.where.not(:firstname => "Mataddmin").count
	@no_of_reports = Report.all.count
    end
    def users
        @users = User.where.not(:firstname => "Mataddmin")
    end 
    def message
    end
    def reports
        @users = User.where.not(:firstname => "Mataddmin")
        @report = Report.all
    end
    def delete  
	@user_id = params[:id]
	@user = User.find(@user_id)
	@user.destroy
	@report = Report.where(:user2_id => @user_id)
	@report.each do |r|
	  r.destroy
	end
	@report = Report.where(:user1_id => @user_id)
	@report.each do |r|
	  r.destroy
	end

	redirect_to "/admin"
    end
end
